/**
 * Common extra modules/prototypes/functions used by the platform
 * --------------------------------------------------------------
 * Here can be defined extra functionalities to be used in the application
 *
 * @author Carl OGREN
 * @copyright CitizenMedia 2011-2013
 */

/**
 * Module dependencies
 */

var fs = require('fs'),					// Node filesystem handlers
	http = require('http'),				// HTTP Server lib
	qstr = require('querystring'),		// QueryString lib
	extend = require('extend'),			// jQuery.extend port
	crypto = require('crypto'),			// encrypt strings
	colorize = require('colorize'),		// Print colors to console
	dateFormat = require('dateformat');	// Format a date




/**
 * String.prototype.encodeHTML
 * -----------------------
 * HTML encoding is simply replacing &, ", < and > chars with their entity equivalents. Order matters, if you don't replace the & chars first, you'll double encode some of the entities:
 *
 * @author Diono (by Johan B.W.)
 * 
 */
String.prototype.encodeHTML = function () {
	return this.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/"/g, '&quot;')
		.replace(/&amp;amp;/g, '&amp;')
		.replace(/&amp;lt;/g, '&lt;')
		.replace(/&amp;gt;/g, '&gt;')
		.replace(/&amp;quot;/g, '&quot;');
};



/**
 * String.prototype.decodeHTML
 * -----------------------
 * Conversely if you want to decode HTML entities[1], make sure you decode &amp; to & after everything else so that you don't double decode any entities:
 *
 * @author Diono (by Johan B.W.)
 * 
 */
String.prototype.decodeHTML = function () {
	return this.replace(/&quot;/g, '"')
		.replace(/&gt;/g, '>')
		.replace(/&lt;/g, '<')
		.replace(/&amp;/g, '&');
};



/**
 * Log information to available console
 * @param  {mixed} type Integer or string expressing the log level
 * @param  {string} msg  Message to print to the console
 * @return {[type]}      [description]
 */
var log = function(type, msg) {
	var typeColor = "",
		result = "";

	// Determine log type
	type = type || "log";

	if (typeof type == 'number') {
		if (type > log.level) { return; }
		type = log.levels[type - 1] || "log";
	} else if (log.level < log[type.toUpperCase()]) {
		return;
	}

	// Retrieve log type color
	typeColor = log.colors[log.levels.indexOf(type)];

	// Retrieve current date
	var date = new Date();

	// Start formating printed message
	result = "#green[>>] #" + typeColor + "[" + type;

	// adapt the number of spaces after the log type
	for (var i = 6 - type.length; i > 0; i--) { result += ' '; }

	// Finish formating printed message
	result += "-] " + dateFormat(date, "isoDate") + " " + dateFormat(date, "isoTime") + " - " + msg;

	// Colorize
	result = colorize.ansify(result);

	// Log message to available console
	console.log(result);
};

// Log levels and their corresponding colors
log.levels = ['error', 'warn', 'info', 'debug'];
log.colors = ['red', 'yellow', 'cyan', 'white'];

// Log level constants
log.ERROR = 1;
log.WARN = 2;
log.INFO = 3;
log.LOG = 3;
log.DEBUG = 4;

// Selected log level
log.level = log.DEBUG;

// export
this.log = log;



/**
 * Send an HTTP POST reques
 * @param  {object} options Request parameters
 */
this.post = function(options) {
	// declare variables
	var defaultOptions = {
			host: 'localhost',
			port: 80,
			path: '/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': 0
			},
			data: {},
			on: {
				success: function(res) {},
				error: function(e) {}
			}
		},
		postData = {},
		events = {};

	// extend default options
	options = extend(defaultOptions, options);

	// extract event callbacks from options
	events = options.on;
	delete options.on;

	// extract post data from options
	postData = qstr.stringify(options.data);
	delete options.data;

	// set content-length header
	options.headers['Content-Length'] = postData.length;

	// init http request object
	var req = http.request(options, function(res) {
			var body = "";

			res.on('data', function(chunk) {
				body += chunk;
			});

			res.on('end', function() {
				events.success(body);
			});
		}).on('error', function(e) {
			events.error(e);
		});

	req.write(postData);
	req.end();
};



/**
 * Hash a string into a selected encoding i.e. md5, sha1 ...
 * @param  {string} encoding Encoding name
 * @param  {string} value    Value to encode
 * @return {string}          Encoded version of the given value
 */
this.hash = function(encoding, value) {
	var hash = crypto.createHash(encoding);
	hash.update(value);
	return hash.digest('hex');
};




this.print_sock = function() {
	console.log("                                                                   ");
	console.log("                                `@@@+                              ");
	console.log("                               @@@@@@@,                            ");
	console.log("                              @@@@#@@@@                            ");
	console.log("                                                                   ");
	console.log("                                 .++                      '@@@+    ");
	console.log("                                @@@+#                   '@@@@@@@   ");
	console.log("                               ;@@@` @                  #+`   +@@  ");
	console.log("                              # '                               @  ");
	console.log("                                                                   ");
	console.log("                             ,       '+@@@'                        ");
	console.log("                             ,       #'@+##@@@,                    ");
	console.log("                              :     @,:+@#+++#@@#                  ");
	console.log("                              .@,,@#+:+.'#@@+++++@+    :@@#.       ");
	console.log("                                  ''.';;;:;@#@#++++@`@      @      ");
	console.log("                                  '#;#.'@.'..@+#@@++,        '     ");
	console.log("                                 @'.'',':''#..#'@+++         #     ");
	console.log("                               @ '''@.'@.''.''.'',#          #     ");
	console.log("                              #'@#'';;':''@.'@,'#.;          @     ");
	console.log("                             `,@';'@,'@.'',''.'';;                 ");
	console.log("                             `'+@'','',''@.'@,''.'@`   '@   #      ");
	console.log("                              #+#'@''@.'',''.''#''@@# @@@@ @       ");
	console.log("                               #@+'''.''@''@;''.'+@@@#@@@@#        ");
	console.log("       `'+++',                 @@`@'@''',''.''@''@'@@@.#@          ");
	console.log("     #;    ''''#+              @@@ @'''#''@'''.''''.               ");
	console.log("   +.       +'''''#           #@@@@.`@'''''''@''@'@                ");
	console.log("  '         '''''''+;        .@@@@@@+ `@#'''''''''`                ");
	console.log("  `         '''''''''#       #@@@@@@@@   '@#'''''+@                ");
	console.log(" `          ;'''''''''@    .#@'#@@@@@@@@   .#@@@@;'#               ");
	console.log(" `          +''''''''''+@@#'+:::::@@@@@@@@+'''''','@               ");
	console.log("  :         '''''''''''''''';::::::;@@+'''''''''@#`                ");
	console.log("  +        ,'''''''''''''''#:::::'@+''''''''''+                    ");
	console.log("   @      `'''''''''''''''#::::@#'''''''''''''#                    ");
	console.log("    +     +'''''''''''''++::;@''''''''''''''''                     ");
	console.log("     #   ;'''''''''+'''@+:'@'''''''''''''''''#                     ");
	console.log("      # ;''''''''+'.''@@@@''''''''''''''''''+                      ");
	console.log("       ;+'''''''':#,#@@@''''''''''''''''''''+                      ");
	console.log("         #'''''''##.+@'''''''''''''''''''''#                       ");
	console.log("          `#'''''''@,'''''''''''''''''''''',                       ");
	console.log("            `#+''''''+''''''''''''''''''''@                        ");
	console.log("               :#''''''''''''''''''''''''+                         ");
	console.log("                  ;#+''''''''''''''''''''.                         ");
	console.log("                     .+#'''''''''''''''#                           ");
	console.log("                         `;''''''''#@;                             ");
	console.log("                                                                   ");
};