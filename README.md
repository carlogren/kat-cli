# Kat-CLI :: The Kat Command Line Interface

----

This project provides a Node.js CLI for the Kat bootstrap project to help initializing projects, modules, libraries, etc.

Command examples :

```
kat init [project name]
```

```
kat module init [module name]
```

```
kat lib init [library name]
```