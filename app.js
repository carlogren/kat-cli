#! /usr/bin/env node
/**
 * Kat-CLI
 * -------
 * Kat-CLI is the Kat bootstrap helper to simplify the
 * creation of a Kat project
 */

var version = "0.1.0";


var $ = require('./lib/common'),	// Common lib
	program = require('commander');	// Command line tool library


/**
 * Process the arguments given
 */
program
	.version(version)
	.parse(process.argv);


/**
 * ========================================
 * Parse user input to execute Kat commands
 * ========================================
 */


// first command
var cmd = program.args[0] || "";

// sub command
var sub_cmd = program.args[1] || "";

// Control that the first command parameter is given
if (cmd == "") {
	$.log("error", "Missing command @todo print help");
	return;
}


else if (cmd == "init") {

	$.log("debug", "Triggered init command");

	// @todo wget and extract the Kat distribution repository
	
	// @todo copy the repository into the current directory

}


else if (cmd == "module") {

	$.log("debug", "Triggered module command");

	$.log("debug", "Looking for sub command");

	if (sub_cmd == "") {
		$.log("error", "Missing module command @todo print module help");
		return;
	}

	$.log("debug", "Found module command : " + sub_cmd);

}


