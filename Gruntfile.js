module.exports = function(grunt) {
	// Project configuration
	grunt.initConfig({
		"pkg": grunt.file.readJSON("package.json"),

		"jshint": {
			"all": [
				"Gruntfile.js",

				// Application Core
				"app.js",
				"config.js",

				// Application libs
				"lib/**/*.js",

				// Application tests
				"test/**/*.js"
			]
		},

		"nodeunit": {
			"all": ["test/**/*.test.js"]
		},

		"watch": {
			"files": ["**/*.js"],
			"tasks": ["jshint"]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-nodeunit');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask("default", ["jshint", "nodeunit"]);
};